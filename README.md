## DGS üben


Currently I'm learning german sign language (Deutsche Gebärdensprache / DGS) and 
I was looking for a tool to practice. Since I didn't find anything that matched 
my needs, I built my own learning tool: It uses the vocabulary I've already learned 
to build random sentences about different topics in the syntax of the german sign 
language.
You can take a look at the learning tool via [opulantus.gitlab.io/dgs-ueben](http://opulantus.gitlab.io/dgs-ueben/).


##### What did I use to build the tool?

The logic for the sentence-generator is written in JavaScript.
The styling of the tool is realized with the CSS-framework [Bulma](https://bulma.io/)