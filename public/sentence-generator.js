////////////////////////////////// VOCABLES //////////////////////////////////////

// Time-elements
let time = ["Woche", "Monat", "Jahr",
    "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag",
    "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
let timePlus = ["Letzte/n/s", "Nächste/n/s"];
let timeFull = ["Gestern", "Heute", "Morgen"];
let timeOfDay = ["frühen Morgen", "Morgen", "Vormittag", "Mittag", "Nachmittag", "Abend", "Nacht"];

// Subjects
let pers = ["die Katze", "der Hund", "die Frau", "der Mann", "das Kind", "das Mädchen", "der Junge"];
let persWithPoss = ["Mutter", "Vater", "Schwester", "Bruder", "Oma", "Opa", "Tochter", "Sohn", "Tante", "Onkel", "Ehefrau", "Ehemann", "Katze"];

// Pronouns
let possPron = ["mein/e", "dein/e", "sein/e", "ihr/e (Sg.)", "unser/e", "euer/e", "ihr/e (Pl.)"];
let persPron = ["ich", "du", "sie (Sg.)", "er", "wir", "ihr", "sie (Pl.)"];

// Objects
let food = ["Schokolade", "Spaghetti", "Pizza", "Wurst", "Baguette", "Banane", "Croissant", "Brot"];
let beverages = ["Wasser", "Saft", "Milch", "Tee", "Kaffee", "Cappuccino"];
let clothes = ["Bluse", "Hemd", "T-Shirt", "Pullover", "Jacke", "Kleid", "Hose", "Rock", "Gürtel", "Mütze"];
let colors = ["gelb", "orange", "rot", "lila", "violett", "blau", "grün", "braun", "beige", "grau", "rosa", "türkis"];
let buildings = ["Haus", "Hochhaus", "Mietwohnung"];
let cities = ["Großstadt", "Kleinstadt", "Dorf", "Zentrum"];
let places = ["München", "Hamburg", "Deutschland", "Bayern"];
let vehicles = ["zu Fuß", "Fahrrad", "Motorrad", "Auto", "Bahn", "Tram"];
let hobby = "Hobby";
let hobbies = ["Shopping", "Spazierengehen", "Jogging", "Sport", "Fernsehen", "Flöte spielen"];
let calendar = ["Tag", "Woche", "Monat", "Jahr"];
let clock = ["Stunde", "Minute", "Sekunde"];

// Descriptions
let colorsFull = ["weiß", "schwarz", "silbern", "golden"];
let colorsPlus = ["hell", "dunkel"];
let adj = ["schön", "alt", "neu"];

// Verbs (for the different topics)
let verbsCC = ["kaufen", "waschen"];
let verbsFB = ["essen", "trinken"];
let verbsL = ["wohnen", "leben", "mieten"];
let verbsT = ["gehen", "fahren"];
let verbsTD = ["(her)kommen", "heimgehen/-fahren"]; // verbs of movement with direction

// Prepositions
let prepos = ["vor", "nach"];

// Question words
let questions = ["was", "was-machen", "wo", "wie", "wieviel Uhr", "wie spät"];

// More Words and phrases
let additionalVoc = ["Tag", "Geburtstag", "Feier", "Party",
    "wichtig", "richtig", "kaputt", "ledig", "praktisch",
    "haben", "arbeiten", "machen",
    "stimmt", "ja", "bitte", "nochmal/auch", "danke", "nein/nicht/nie/niemals"];
let additionalPhr = ["Hau ab!", "Mir Wurst.", "Bitte nochmal.", "Bitte wiederholen.", "Vielen Dank.", "Zum Beispiel."];


/////////////////////////////////////// LOGIC ////////////////////////////////////////////

////// Function to build random sentences about a certain topic //////
function getSentence(topic) {

    //// This first part is common for the different topics ////

    // Variable to store the generated sentence
    let sen;

    // Variable to store the first part of the sentence
    let firstPart;

    // Variables to store the random values for building the sentence
    let te;     // Random time-element
    let s;      // Random subject

    // Get a random time-element
    let t = [time, timeFull, timeOfDay];
    let tr = getRandomElement(t);
    if (tr === time) {
        te = getRandomElement(timePlus) + " " + getRandomElement(time);
    } else {
        te = getRandomElement(timeFull);
    }

    // Get a random subject
    let p = [pers, persWithPoss, persPron];
    let pr = getRandomElement(p);
    if (pr === persWithPoss) {
        s = getRandomElement(possPron) + " " + getRandomElement(persWithPoss);
    } else {
        s = getRandomElement(pr);
    }

    // Put together the first part of the sentence (with or without time-element (50/50 chance))
    if (coinFlip() === true) {
        firstPart = te + " " + s + " ";
    } else {
        firstPart = s + " ";
    }


    //// This second part is distinctive for the different topics ////

    // Topic: Clothes and Colors
    if (topic === "cc") {
        // Get a random description: either a color or a color with an addition (dark/light (50/50 chance))
        let d;      // Random description
        let des = [colors, colorsFull, adj];
        let dr = getRandomElement(des);
        if (dr === colors) {
            if (coinFlip() === true) {
                d = getRandomElement(colorsPlus) + getRandomElement(colors);
            } else {
                d = getRandomElement(colors);
            }
        } else {
            d = getRandomElement(dr);
        }
        // Put the different Elements together
        sen = firstPart + getRandomElement(clothes) + " " + d + " " + getRandomElement(verbsCC) + ".";
    }

    // Topic: Food and Beverages
    if (topic === "fb") {
        let f = [food, beverages];
        let fr = getRandomElement(f);
        let i; // Index for picking the right verb, set in the following if-statement
        if (fr === food) {
            i = 0;
        } else if (fr === beverages) {
            i = 1;
        }
        sen = firstPart + getRandomElement(fr) + " " + verbsFB[i] + ".";
    }

    // Topic: Living and Traveling
    if (topic === "lt") {
        if (coinFlip() === true) { // 50/50 chance for living or travel
            let p = [places, cities, buildings];
            let pr = getRandomElement(p);
            if (pr === buildings) {
                sen = s + " " + getRandomElement(pr) + " " + verbsL[0] + ".";
            } else {
                sen = s + " " + getRandomElement(pr) + " " + verbsL[1] + ".";
            }
        } else {
            if (coinFlip() === true) {
                let vr = getRandomElement(vehicles);
                let v;
                if (vr === vehicles[0]) {
                    v = verbsT[0];
                } else {
                    v = verbsT[1];
                }
                sen = firstPart + vr + " " + v + ".";
            } else {
                sen = firstPart + getRandomElement(places) + " " + verbsT[1] + ".";
            }
        }
    }

    // Topic: Hobbies
    if (topic === "ho") {
        sen = getRandomElement(possPron) + " " + hobby + " " + getRandomElement(hobbies) + ".";
    }

     // Topic: Numbers and Calendar vocabulary
    if (topic === "nc") {
    let num;
    	// Pick a section of numbers
    	if (coinFlip() === true) {
    	num = getRandomInt(1,19);
    	} else {
    	num = getRandomInt(20,1000);
    	}
    	// Pick calendar word and form sentence
    	sen = getRandomElement(timePlus) + " " + num + " " + getRandomElement(calendar);
		}

     // Topic: Time
    if (topic === "cl") {
        // Pick a random category of time-sentence
        let ran = getRandomInt(1,3);
        // General time information
        if (ran === 1) {
            // Pick corresponding hour and time words
            let hour = getRandomInt(1,12);
            let daytime;
            switch (hour) {
                case 1:
                case 2:
                case 3:
                case 4:
                if (coinFlip) {
                    daytime = timeOfDay[4];
                } else {
                    daytime = timeOfDay[6];
                }
                break;
                case 5:
                case 6:
                if (coinFlip) {
                    daytime = timeOfDay[1];
                } else {
                    daytime = timeOfDay[4];
                }
                break;
                case 7:
                case 8:
                case 9:
                if (coinFlip) {
                    daytime = timeOfDay[1];
                } else {
                    daytime = timeOfDay[5];
                }
                break;
                case 10:
                case 11:
                if (coinFlip) {
                    daytime = timeOfDay[2];
                } else {
                    daytime = timeOfDay[5];
                }
                break;
                default:
                if (coinFlip) {
                    daytime = timeOfDay[3];
                } else {
                    daytime = timeOfDay[6];
                }
                break;
            }
            if (coinFlip) {
                if (coinFlip){
                    if (coinFlip){
                        let quarter = [15, 45];
                        sen = hour + "-Uhr " + getRandomElement(quarter) + " " + daytime;
                    } else {
                        sen = "halb " + hour + "-Uhr " + daytime;
                    }
                } else {
                   sen = hour+"-Uhr " + daytime; 
                }
            } else {
                sen = hour + "-Uhr kurz " + getRandomElement(prepos);
                if (coinFlip) {
                    sen = "halb " + sen;
                }
            }            
        } 
        // Precise time information
        else if (ran === 2) {
            sen = getRandomInt(0,23) + " Uhr " + pad(getRandomInt(0,59),2);
        } 
        // Information about duration
        else if (ran === 3) {
            let num;
    	    // Pick a section of numbers
    	    if (coinFlip() === true) {
    	        num = getRandomInt(1,19);
    	    } else {
    	        num = getRandomInt(20,60);
    	    }
    	    // Pick time word and form sentence
    	   	sen = num + " " + getRandomElement(clock);
        } 
    }

    // Topic: Questions
    if (topic === "qu") {
        // Pick a random question particle
        let qr = getRandomElement(questions);
        // What-questions
        if (qr === questions[0]) {
            if (coinFlip() === true) {
                let v = verbsFB.concat(verbsCC);
                sen = firstPart + getRandomElement(v) + " " + questions[0] + "?";
            } else {
                sen = getRandomElement(possPron) + " " + hobby + " " + questions[0] + "?";
            }
            // What-do/does-question
        } else if (qr === questions[1]) {
            sen = firstPart + questions[1] + "?";
            // Where-questions
        } else if (qr === questions[2]) {
            sen = firstPart + getRandomElement(verbsT) + " " + questions[2] + "?";
            // How-questions
        } else if (qr === questions[3]) {
            sen = firstPart + getRandomElement(verbsTD) + " " + questions[3] + "?";
        } else {
            sen = qr + "?";
        }
    }

    // Put sentence into HTML
    setWordOrSentence("sentence", sen);
}


////// Function to pick a single Word from the vocables which are not used to build sentences //////
function getWord(wordOrPhrase) {
    let randomElement;
    if (wordOrPhrase === "w") {
        randomElement = getRandomElement(additionalVoc);
    } else if (wordOrPhrase === "p") {
        randomElement = getRandomElement(additionalPhr)
    }
    setWordOrSentence("word", randomElement)
}


//// Functions used by the above logic ////

// General function to put a selected word or generated sentence into the HTML
function setWordOrSentence(elementId, wordOrSentence) {
    document.getElementById(elementId).innerHTML = wordOrSentence;
}

// General function to pick a random Element from an Array
function getRandomElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

// Function for 50/50 random choices, returns true or false
function coinFlip() {
    return (Math.floor(Math.random() * 2) === 0);
}

// Function for Random Numbers
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function for leading zeros (for precise time information)
function pad(num, size) {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
}
